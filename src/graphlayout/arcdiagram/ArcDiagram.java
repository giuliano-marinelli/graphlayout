package graphlayout.arcdiagram;

import java.util.LinkedList;

/**
 *
 * @author Giuliano Marinelli
 */
public class ArcDiagram {

    private int[] nodes;
    private int[] order;
    //[i][0] = nodeA, [i][1] = nodeB, [i][2] = direction
    private int[][] edges;

    public ArcDiagram() {
    }

    public ArcDiagram(int[] nodes, int[] order, int[][] edges) {
        this.nodes = nodes;
        this.order = order;
        this.edges = edges;
    }

    public int[] getNodes() {
        return nodes;
    }

    public void setNodes(int[] nodes) {
        this.nodes = nodes;
    }

    public int[] getOrder() {
        return order;
    }

    public void setOrder(int[] order) {
        this.order = order;
    }

    public int[][] getEdges() {
        return edges;
    }

    public void setEdges(int[][] edges) {
        this.edges = edges;
    }

    public void moveNode(int node, int nodeIndex) {
        swapNodes(node, nodes[nodeIndex]);
    }

    public void swapNodes(int nodeA, int nodeB) {
        if (nodeA != nodeB) {
            int orderA = -1;
            int orderB = -1;
            int i = 0;
            while (i < nodes.length && (orderA == -1 || orderB == -1)) {
                if (nodes[i] == nodeA) {
                    orderA = i;
                    nodes[i] = nodeB;
                } else if (nodes[i] == nodeB) {
                    orderB = i;
                    nodes[i] = nodeA;
                }
                i++;
            }
            order[nodeB] = orderA;
            order[nodeA] = orderB;
        }
    }

    public LinkedList<Integer> nodeEdges(int node) {
        LinkedList<Integer> nodeEdges = new LinkedList<>();
        for (int i = 0; i < edges.length; i++) {
            if (edges[i][0] == node || edges[i][1] == node) {
                nodeEdges.add(i);
            }
        }
        return nodeEdges;
    }

    public boolean existsEdge(int nodeA, int nodeB) {
        int edgeIndex = -1;
        int i = 0;
        while (i < edges.length && edgeIndex == -1) {
            if ((edges[i][0] == nodeA && edges[i][1] == nodeB)
                    || (edges[i][0] == nodeB && edges[i][1] == nodeA)) {
                edgeIndex = i;
            }
            i++;
        }
        return edgeIndex != -1;
    }

    public void directionEdge(int edgeIndex, int direction) {
        edges[edgeIndex][2] = direction;
    }

    public void invertEdge(int edgeIndex) {
        edges[edgeIndex][2] = (edges[edgeIndex][2] == 0) ? 1 : 0;
    }

    public void invertEdge(int nodeA, int nodeB) {
        int edgeIndex = -1;
        int i = 0;
        while (i < edges.length && edgeIndex == -1) {
            if ((edges[i][0] == nodeA && edges[i][1] == nodeB)
                    || (edges[i][0] == nodeB && edges[i][1] == nodeA)) {
                edgeIndex = i;
            }
            i++;
        }
        if (edgeIndex != -1) {
            invertEdge(edgeIndex);
        }
    }

    public LinkedList<Integer> crossingEdges(int edgeIndex) {
        LinkedList<Integer> crossEdges = new LinkedList<>();
        int ordEdgeANodeA = order[edges[edgeIndex][0]];
        int ordEdgeANodeB = order[edges[edgeIndex][1]];
        int ordEdgeBNodeA;
        int ordEdgeBNodeB;
        for (int i = 0; i < edges.length; i++) {
            if (edges[edgeIndex][2] == edges[i][2] && i != edgeIndex) {
                ordEdgeBNodeA = order[edges[i][0]];
                ordEdgeBNodeB = order[edges[i][1]];
                if (Math.min(ordEdgeANodeA, ordEdgeANodeB) < Math.min(ordEdgeBNodeA, ordEdgeBNodeB)
                        && Math.max(ordEdgeANodeA, ordEdgeANodeB) < Math.max(ordEdgeBNodeA, ordEdgeBNodeB)
                        && Math.max(ordEdgeANodeA, ordEdgeANodeB) > Math.min(ordEdgeBNodeA, ordEdgeBNodeB)) {
                    crossEdges.add(i);
                }
            }
        }
        return crossEdges;
    }

    public int crossingNumber() {
        //System.out.println("CROSSINGS");
        int cross = 0;
        int ordEdgeANodeA;
        int ordEdgeANodeB;
        int ordEdgeBNodeA;
        int ordEdgeBNodeB;
        int edgeAmin;
        int edgeAmax;
        int edgeBmin;
        int edgeBmax;
        for (int i = 0; i < edges.length; i++) {
            ordEdgeANodeA = order[edges[i][0]];
            ordEdgeANodeB = order[edges[i][1]];
            edgeAmin = Math.min(ordEdgeANodeA, ordEdgeANodeB);
            edgeAmax = Math.max(ordEdgeANodeA, ordEdgeANodeB);
            for (int j = i + 1; j < edges.length; j++) {
                if (edges[i][2] == edges[j][2]) {
                    ordEdgeBNodeA = order[edges[j][0]];
                    ordEdgeBNodeB = order[edges[j][1]];
                    edgeBmin = Math.min(ordEdgeBNodeA, ordEdgeBNodeB);
                    edgeBmax = Math.max(ordEdgeBNodeA, ordEdgeBNodeB);
                    if ((edgeAmin < edgeBmin && edgeAmax < edgeBmax && edgeBmin < edgeAmax)
                            || (edgeBmin < edgeAmin && edgeBmax < edgeAmax && edgeAmin < edgeBmax)) {
                        //System.out.print("C: (Am," + edgeAmin + ")(AM," + edgeAmax + ")(Bm," + edgeBmin + ")(BM," + edgeBmax + ")");
                        //System.out.println(" | CV: (Am," + edges[i][0] + ")(AM," + edges[i][1] + ")(Bm," + edges[j][0] + ")(BM," + edges[j][1] + ")");
                        cross++;
                    }
                }
            }
        }
        return cross;
    }

    public int nodeDegree(int node) {
        int degree = 0;
        for (int i = 0; i < edges.length; i++) {
            if (edges[i][0] == node || edges[i][1] == node) {
                degree++;
            }
        }
        return degree;
    }

    public int edgeDegree(int edgeIndex) {
        return Math.max(nodeDegree(edges[edgeIndex][0]), nodeDegree(edges[edgeIndex][1]));
    }

    public int nodeLevel(int node) {
        int level = Integer.MAX_VALUE;
        int edgeDegree;
        for (int i = 0; i < edges.length; i++) {
            if (edges[i][0] == node || edges[i][1] == node) {
                edgeDegree = edgeDegree(i);
                if (edgeDegree < level) {
                    level = edgeDegree;
                }
            }
        }
        return level;
    }

    public ArcDiagram clone() {
        ArcDiagram clon = new ArcDiagram();
        int[][] clonEdges = new int[edges.length][edges[0].length];
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[0].length; j++) {
                clonEdges[i][j] = edges[i][j];
            }
        }
        clon.setEdges(clonEdges);
        clon.setNodes(nodes.clone());
        clon.setOrder(order.clone());
        return clon;
    }

    public String toString() {
        String result;
        result = "[" + nodes[0];
        for (int i = 1; i < nodes.length; i++) {
            result += ", " + nodes[i];
        }
        result += "][" + order[0];
        for (int i = 1; i < order.length; i++) {
            result += ", " + order[i];
        }
        result += "][" + edges[0][0] + edges[0][1] + "|" + edges[0][2];
        for (int i = 1; i < edges.length; i++) {
            result += ", " + edges[i][0] + edges[i][1] + "|" + edges[i][2];
        }
        result += "]";
        return result;
    }

}
