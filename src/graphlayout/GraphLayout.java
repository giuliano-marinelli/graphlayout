package graphlayout;

import fdp.Parameter;
import graphlayout.arcdiagram.ArcDiagram;
import graphlayout.gui.ArcDiagramPainter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import fdp.Simulation;
import fdp.graph.Edge;
import fdp.graph.EdgeFactory;
import fdp.graph.Vertex;
import graphlayout.gui.GenericGraphPainter;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.vecmath.Vector2d;
import org.jgrapht.Graph;
import org.jgrapht.graph.SimpleGraph;
import parsii.tokenizer.ParseException;

/**
 *
 * @author Giuliano Marinelli
 */
public class GraphLayout {

    private static int cycles;
    private static double timeLineal;
    private static double timeGenetic;
    private static double timeForceDirected;
    private static double timeArcDiagram2Generic;

    public static int getCycles() {
        return cycles;
    }

    public static double getTime() {
        return timeGenetic;
    }

    public static void main(String[] args) throws ParseException, Exception {
        //grafos iniciales
        ArcDiagram graph = linearOptimization(generateRandomGraph(15));
        //ArcGraph graph = importGraph(new File("src/files/cr.xml"));

        //visualizar graficamente
        ArcDiagramPainter graphPainter = new ArcDiagramPainter(graph);
        //GENERACION DE RESULTADOS
        //for (int i = 5; i <= 50; i = i + 5) {
            //calculateResults(i, 100);
            //graph = linearOptimization(generateCompleteGraph(i));
            //System.out.println("K_" + i + " | Obt: " + graph.crossingNumber() + " Real: " + completeGraphCrossingNumber(i));
        //}
        //savePromedies();
        //TEST Generic Graph con Force Directed Simulation
        //ArcDiagram arcDiagram = importGraph(new File("src/files/test-example.xml"));
        /*ArcDiagram arcDiagram = generateRandomGraph(25);
         System.out.println(arcDiagram.toString());

         HashMap<Vertex, String> vertex2Nodes = new HashMap<>();
         Graph<Vertex, Edge> genericGraph = arcDiagramToGenericGraph(arcDiagram, vertex2Nodes);
         for (Vertex vertex : genericGraph.vertexSet()) {
         System.out.println(vertex.getPos().toString());
         }
         for (Edge edge : genericGraph.edgeSet()) {
         System.out.println(vertex2Nodes.get(edge.getV()) + " -> " + vertex2Nodes.get(edge.getU()));
         }

         Parameter parameter = new Parameter();
         parameter.setFrameWidth(500);
         parameter.setFrameHeight(500);
         parameter.setEquilibriumCriterion(true);
         parameter.setCriterion(5);
         parameter.setCoolingRate(0.1);
         parameter.setFrameDelay(5);
         parameter.setAttractiveForce("(d * d) / k");
         parameter.setRepulsiveForce("(k * k) / d");

         Simulation fdpSimulation = new Simulation(genericGraph, parameter);
         fdpSimulation.call();

         new GenericGraphPainter(genericGraph, vertex2Nodes);

         arcDiagram = geneticOptimization(linearOptimization(arcDiagram));

         vertex2Nodes = new HashMap<>();
         genericGraph = arcDiagramToGenericGraph(arcDiagram, vertex2Nodes);

         fdpSimulation = new Simulation(genericGraph, parameter);
         fdpSimulation.call();

         new GenericGraphPainter(genericGraph, vertex2Nodes);*/

        /*
         Graph<Vertex, Edge> fdpGraph = new SimpleGraph<>(new EdgeFactory());
         Vertex v1 = new Vertex();
         v1.setPos(new Vector2d(0,0));
         fdpGraph.addVertex(v1);
        
         Vertex v2 = new Vertex();
         v2.setPos(new Vector2d(0,20));
         fdpGraph.addVertex(v2);
        
         Parameter parameter = new Parameter();
         parameter.setFrameWidth(500);
         parameter.setFrameHeight(500);
         parameter.setEquilibriumCriterion(true);
         parameter.setCriterion(5);
         parameter.setCoolingRate(0.065);
         parameter.setFrameDelay(5);
         parameter.setAttractiveForce("(d * d) / k");
         parameter.setRepulsiveForce("(k * k) / d");
        
         Simulation fdpSimulation = new Simulation(fdpGraph, parameter);
         for (Vertex vertex : fdpGraph.vertexSet()) {
         System.out.println(vertex.getPos().toString());
         }
         fdpSimulation.call();
         for (Vertex vertex : fdpGraph.vertexSet()) {
         System.out.println(vertex.getPos().toString());
         }*/
    }

    public static void initSimulateConcentricDirected(Graph<Vertex, Edge> genericGraph, HashMap<Point, Integer> grid, Point gridCentroid, int gridDistance) {
        //genero una grilla de posiciones basado en la cantidad de nodos
        int gridSize = (int) Math.ceil(Math.sqrt(genericGraph.vertexSet().size())) + 1;

        //obtengo el centroide de los nodos
        double centroidX = 0;
        double centroidY = 0;
        for (Vertex vertex : genericGraph.vertexSet()) {
            centroidX += vertex.getPos().x;
            centroidY += vertex.getPos().y;
        }
        Point centroid = new Point((int) centroidX / genericGraph.vertexSet().size(), (int) centroidY / genericGraph.vertexSet().size());
        //ajusto el centroide a la escala de la grilla (gridDistance)
        gridCentroid.setLocation(new Point(Math.round(centroid.x / gridDistance) * gridDistance, Math.round(centroid.y / gridDistance) * gridDistance));

        //genero los demas puntos de la grilla en base al centroide ajustado
        int maxDistance = (int) Math.round(gridCentroid.distance(
                (new Point(
                        gridDistance * (gridSize / 2 + 1),
                        gridDistance * (gridSize / 2 + 1)))));
        grid.put(gridCentroid, maxDistance);
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                int xDistance = gridDistance * Math.abs(i - gridSize / 2);
                int yDistance = gridDistance * Math.abs(j - gridSize / 2);
                if (xDistance > 0 || yDistance > 0) {
                    Point gridPoint = new Point(
                            i < gridSize / 2 ? gridCentroid.x - xDistance : gridCentroid.x + xDistance,
                            j < gridSize / 2 ? gridCentroid.y - yDistance : gridCentroid.y + yDistance);
//                    Vertex chosenVertex = null;
//                    for (Vertex vertex : genericGraph.vertexSet()) {
//                        if (vertex.getPos().x == gridPoint.x && vertex.getPos().y == gridPoint.y) {
//                            chosenVertex = vertex;
//                        }
//                    }
//                    grid.put(gridPoint, chosenVertex);
                    grid.put(gridPoint, maxDistance - (int) Math.round(gridCentroid.distance(gridPoint)));
                }
            }
        }
    }

    public static void iterateSimulateConcentricDirected(Graph<Vertex, Edge> genericGraph, HashMap<Point, Integer> grid, Point gridCentroid, int gridDistance) {
        HashMap<Point, Boolean> taken = new HashMap<>();
        for (Vertex vertex : genericGraph.vertexSet()) {
            Point vertexPoint = new Point((int) vertex.getPos().x, (int) vertex.getPos().y);
            taken.put(vertexPoint, true);
        }

        for (Vertex vertex : genericGraph.vertexSet()) {
            Point vertexPoint = new Point((int) vertex.getPos().x, (int) vertex.getPos().y);
            Point chosenPoint = new Point(0, 0);
            int chosenAttraction = Integer.MIN_VALUE;
            int newX = vertexPoint.x;
            int newY = vertexPoint.y;
//            System.out.println("FROM: (" + vertex.getPos().x + "," + vertex.getPos().y + ")");
            for (Map.Entry<Point, Integer> gridEntry : grid.entrySet()) {
                Point gridPoint = gridEntry.getKey();
                Integer gridForce = gridEntry.getValue();
                int gridAttraction = gridForce;//(int) Math.round(gridForce - vertexPoint.distance(gridPoint));
//                System.out.print("[" + chosenAttraction + "] TO: (" + gridPoint.x + "," + gridPoint.y + ") FORCE: " + gridForce + " DIST: " + gridDistance + " ATTRACT: " + gridAttraction);
                if (gridAttraction > chosenAttraction) {
                    int posibleNewX = gridPoint.x >= vertexPoint.x
                            ? gridPoint.x == vertexPoint.x ? vertexPoint.x : vertexPoint.x + gridDistance
                            : vertexPoint.x - gridDistance;
                    int posibleNewY = gridPoint.y >= vertexPoint.y
                            ? gridPoint.y == vertexPoint.y ? vertexPoint.y : vertexPoint.y + gridDistance
                            : vertexPoint.y - gridDistance;
//                    if (Math.abs(vertexPoint.x - gridPoint.x) < Math.abs(vertexPoint.y - gridPoint.y) && posibleNewX != vertexPoint.x) {
//                        posibleNewY = vertexPoint.y;
//                    } else if (Math.abs(vertexPoint.x - gridPoint.x) > Math.abs(vertexPoint.y - gridPoint.y) && posibleNewY != vertexPoint.y) {
//                        posibleNewX = vertexPoint.x;
//                    }
//                    System.out.print(" TARGET: (" + posibleNewX + "," + posibleNewY + ")");
//                    System.out.print(" TAKEN: " + (taken.get(new Point(posibleNewX, posibleNewY)) != null && taken.get(new Point(posibleNewX, posibleNewY))));
                    if ((vertexPoint.x == posibleNewX && vertexPoint.y == posibleNewY)
                            || (taken.get(new Point(posibleNewX, posibleNewY)) == null || !taken.get(new Point(posibleNewX, posibleNewY)))) {
                        newX = posibleNewX;
                        newY = posibleNewY;
                        chosenPoint.setLocation(gridPoint);
                        chosenAttraction = gridAttraction;
//                        System.out.print(" (CHOSEN)");
                    }
                }
//                System.out.println();
            }

            taken.put(vertexPoint, false);
            taken.put(new Point(newX, newY), true);
            vertex.setPos(new Vector2d(newX, newY));
        }
    }

    public static void completeSimulateConcentricDirected(Graph<Vertex, Edge> genericGraph, HashMap<Point, Integer> grid, Point gridCentroid, int gridDistance) {
        boolean allLocated = false;
        int i = 0;
        while (!allLocated || i > 100) {
            boolean iterationLocated = true;
            for (Vertex vertex : genericGraph.vertexSet()) {
                Point vertexPoint = new Point((int) vertex.getPos().x, (int) vertex.getPos().y);
                if (!grid.containsKey(vertexPoint)) {
                    iterationLocated = false;
                }
            }
            allLocated = iterationLocated;
            if (!allLocated) {
                iterateSimulateConcentricDirected(genericGraph, grid, gridCentroid, gridDistance);
            }
            i++;
        }
    }

    public static void simulateForceDirected(Graph<Vertex, Edge> genericGraph) throws ParseException, Exception {
        double startTime = System.currentTimeMillis();
        Parameter parameter = new Parameter();
        parameter.setFrameWidth(500);
        parameter.setFrameHeight(500);
        parameter.setEquilibriumCriterion(true);
        parameter.setCriterion(1);
        parameter.setCoolingRate(0.5);
        parameter.setFrameDelay(2);
        parameter.setAttractiveForce("(d * d) / k");
        parameter.setRepulsiveForce("(k * k) / d");

        Simulation fdpSimulation = new Simulation(genericGraph, parameter);
        fdpSimulation.call();
        double endTime = (System.currentTimeMillis() - startTime) / 1000;
        timeForceDirected = endTime;
    }

    public static Graph<Vertex, Edge> arcDiagramToGenericGraph(ArcDiagram arcDiagram, HashMap<Vertex, String> vertex2Nodes, int gridDistance) {
        double startTime = System.currentTimeMillis();
        Graph<Vertex, Edge> genericGraph = new SimpleGraph<>(new EdgeFactory());

        int xOffset = 0;
        int yOffset = 0;
        HashMap<Integer, Vertex> nodes2Vertex = new HashMap<>();
        for (int i = 0; i < arcDiagram.getNodes().length; i++) {
            Vertex vertex = new Vertex();
            vertex.setPos(new Vector2d(i * gridDistance + xOffset, yOffset));
            genericGraph.addVertex(vertex);
            nodes2Vertex.put(arcDiagram.getNodes()[i], vertex);
            vertex2Nodes.put(vertex, "n" + arcDiagram.getNodes()[i]);
        }

        for (int[] edge : arcDiagram.getEdges()) {
            Vertex vertexA = nodes2Vertex.get(edge[0]);
            Vertex vertexB = nodes2Vertex.get(edge[1]);
            int direction = edge[2];

            int edgeSize = Math.abs(arcDiagram.getOrder()[edge[0]] - arcDiagram.getOrder()[edge[1]]);
            if (edgeSize > 1) {
                Vertex inflectionVertex = new Vertex();
                double xPos = Math.min(vertexA.getPos().x, vertexB.getPos().x) + Math.abs(vertexA.getPos().x - vertexB.getPos().x) / 2;
                double yPos = direction == 0
                        ? yOffset + Math.abs(vertexA.getPos().x - vertexB.getPos().x) / 2
                        : yOffset - Math.abs(vertexA.getPos().x - vertexB.getPos().x) / 2;
                inflectionVertex.setPos(new Vector2d(xPos, yPos));
                genericGraph.addVertex(inflectionVertex);

                genericGraph.addEdge(vertexA, inflectionVertex);
                genericGraph.addEdge(inflectionVertex, vertexB);

                vertex2Nodes.put(inflectionVertex, "i" + edge[0] + "-" + edge[1]);
            } else {
                genericGraph.addEdge(vertexA, vertexB);
            }
        }

        double endTime = (System.currentTimeMillis() - startTime) / 1000;
        timeArcDiagram2Generic = endTime;
        return genericGraph;
    }

    public static Graph<Vertex, Edge> arcDiagramToOrthogonalGraph(ArcDiagram arcDiagram, HashMap<Vertex, String> vertex2Nodes, int gridDistance) {
        Graph<Vertex, Edge> orthogonalGraph = new SimpleGraph<>(new EdgeFactory());

        int xOffset = 0;
        int yOffset = 0;
        //mapea nodos de arcDiagram a vertices de orthogonalGraph
        HashMap<Integer, Vertex> nodes2Vertex = new HashMap<>();

        for (int i = 0; i < arcDiagram.getNodes().length; i++) {
            Vertex vertex = new Vertex();
            vertex.setPos(new Vector2d(i * gridDistance + xOffset, yOffset));
            orthogonalGraph.addVertex(vertex);
            nodes2Vertex.put(arcDiagram.getNodes()[i], vertex);
            vertex2Nodes.put(vertex, "n" + arcDiagram.getNodes()[i]);
        }

        for (int[] edge : arcDiagram.getEdges()) {
            Vertex vertexA = nodes2Vertex.get(edge[0]);
            Vertex vertexB = nodes2Vertex.get(edge[1]);
            int direction = edge[2];

            if (Math.abs(arcDiagram.getOrder()[edge[0]] - arcDiagram.getOrder()[edge[1]]) > 1) {
                Vertex inflectionVertexA = new Vertex();
                Vertex inflectionVertexB = new Vertex();

                Vertex minVertex = vertexA.getPos().x > vertexB.getPos().x ? vertexB : vertexA;
                Vertex maxVertex = vertexA.getPos().x > vertexB.getPos().x ? vertexA : vertexB;

                double xPosA = minVertex.getPos().x;
                double xPosB = maxVertex.getPos().x;
                double yPos = direction == 0
                        ? yOffset + Math.abs(vertexA.getPos().x - vertexB.getPos().x) / 2
                        : yOffset - Math.abs(vertexA.getPos().x - vertexB.getPos().x) / 2;
                inflectionVertexA.setPos(new Vector2d(xPosA, yPos));
                inflectionVertexB.setPos(new Vector2d(xPosB, yPos));

                orthogonalGraph.addVertex(inflectionVertexA);
                orthogonalGraph.addVertex(inflectionVertexB);

                orthogonalGraph.addEdge(minVertex, inflectionVertexA);
                orthogonalGraph.addEdge(inflectionVertexA, inflectionVertexB);
                orthogonalGraph.addEdge(inflectionVertexB, maxVertex);

                vertex2Nodes.put(inflectionVertexA, "i" + edge[0] + "-" + edge[1]);
                vertex2Nodes.put(inflectionVertexB, "i" + edge[1] + "-" + edge[0]);
            } else {
                orthogonalGraph.addEdge(vertexA, vertexB);
            }
        }

        return orthogonalGraph;
    }

    public static ArcDiagram linearOptimization(ArcDiagram graph) {
        double startTime = System.currentTimeMillis();
        //grafo resultado
        ArcDiagram goalGraph = graph.clone();
        int cantNodes = goalGraph.getNodes().length;
        //genera una lista enlazada con el arreglo de nodos
        LinkedList<Integer> nodes = new LinkedList<>();
        for (int i = 0; i < cantNodes; i++) {
            nodes.add(goalGraph.getNodes()[i]);
        }
        //utiliza un método de listas para ordernar los nodos por nivel de menor a mayor
        nodes.sort((o1, o2) -> {
            int result = 0;
            if (goalGraph.nodeLevel(o1) < goalGraph.nodeLevel(o2)) {
                result = -1;
            } else if (goalGraph.nodeLevel(o1) > goalGraph.nodeLevel(o2)) {
                result = 1;
            }
            return result;
        });
        //determina si hay un número par de nodos
        boolean isPar = cantNodes % 2 == 0;
        //puntero a la posición izquierda del arreglo
        int leftPointer = cantNodes / 2;
        //puntero a la posición derecha del arreglo
        int rightPointer = isPar ? cantNodes / 2 - 1 : cantNodes / 2 + 1;
        //determina si colocará el arco por arriba o abajo
        int isTop = cantNodes % 2;
        LinkedList<Integer> nodeEdges;
        //para cada nodo traza sus arcos por arriba o abajo segun isTop
        for (Integer node : nodes) {
            nodeEdges = goalGraph.nodeEdges(node);
            for (Integer edge : nodeEdges) {
                goalGraph.directionEdge(edge, isTop);
            }
            if (isTop == cantNodes % 2) {
                goalGraph.moveNode(node, leftPointer);
                isTop = 1 - cantNodes % 2;
                leftPointer = isPar ? leftPointer + 1 : leftPointer - 1;
            } else {
                goalGraph.moveNode(node, rightPointer);
                isTop = cantNodes % 2;
                rightPointer = isPar ? rightPointer - 1 : rightPointer + 1;
            }
        }
        //llama a optimizar para eliminar conflictos de grafos completos
        ArcDiagram optimizedGraph = completeGraphOptimization(goalGraph);
        double endTime = (System.currentTimeMillis() - startTime) / 1000;
        timeLineal = endTime;
        return optimizedGraph;
    }

    private static ArcDiagram completeGraphOptimization(ArcDiagram graph) {
        //grafo resultado
        ArcDiagram goalGraph = graph.clone();
        //datos del grafo
        int[] nodes = goalGraph.getNodes();
        int[][] edges = goalGraph.getEdges();
        int[] order = goalGraph.getOrder();
        int cantNodes = nodes.length;
        //numero de inversiones
        int optimizeNumber = (cantNodes > 5) ? ((cantNodes / 2) - 2) : 0;
        //int cantChanges = optimizeNumber;
        //punteros
        int leftPointer = 0;
        int rightPointer = cantNodes - 1;
        boolean pointer = true;
        LinkedList<Integer> nodeEdges;
        //para evaluar el crossing number de la inversion
        int crossingNumber = goalGraph.crossingNumber();
        int newCrossingNumber;
        //recorre para cada serie de inversiones
        for (int k = 0; k < optimizeNumber; k++) {
            //obtiene el nodo segun el puntero
            if (pointer) {
                nodeEdges = goalGraph.nodeEdges(nodes[leftPointer]);
                leftPointer++;
                pointer = false;
            } else {
                nodeEdges = goalGraph.nodeEdges(nodes[rightPointer]);
                rightPointer--;
                pointer = true;
            }
            int cantChanges = optimizeNumber - k;
            //para los [cantChanges] arcos del nodo realiza una inversion
            //y chequea que su crossing number haya mejorado
            int i = 0;
            while (i < nodeEdges.size() && cantChanges > 0) {
                int ordEdgeNodeA = order[edges[nodeEdges.get(i)][0]];
                int ordEdgeNodeB = order[edges[nodeEdges.get(i)][1]];
                int nodesDistance = Math.abs(ordEdgeNodeA - ordEdgeNodeB);
                if (nodesDistance > 1 && nodesDistance <= (optimizeNumber - k) + 1) {
                    goalGraph.invertEdge(nodeEdges.get(i));
                    cantChanges--;
                }
                newCrossingNumber = goalGraph.crossingNumber();
                if (crossingNumber < newCrossingNumber) {
                    goalGraph.invertEdge(nodeEdges.get(i));
                } else {
                    crossingNumber = newCrossingNumber;
                }
                i++;
            }
        }
        return goalGraph;
    }

    public static ArcDiagram alternativeLinearOptimization(ArcDiagram graph) {
        ArcDiagram goalGraph = graph.clone();
        int cantNodes = goalGraph.getNodes().length;
        LinkedList<Integer> listNodes = new LinkedList<>();
        for (int i = 0; i < cantNodes; i++) {
            listNodes.add(goalGraph.getNodes()[i]);
        }
        listNodes.sort((o1, o2) -> {
            int result = 0;
            if (goalGraph.nodeLevel(o1) < goalGraph.nodeLevel(o2)) {
                result = -1;
            } else if (goalGraph.nodeLevel(o1) > goalGraph.nodeLevel(o2)) {
                result = 1;
            }
            return result;
        });
        int[] forces = new int[cantNodes];
        int[][] edges = goalGraph.getEdges();
        for (int i = 0; i < edges.length; i++) {
            edges[i][2] = -1;
        }
        boolean isPar = cantNodes % 2 == 0;
        int direction = 1;
        LinkedList<Integer> nodeEdges;
        for (Integer node : listNodes) {
            if (forces[0] == 0) {
                goalGraph.moveNode(node, 0);
                forces[0] = goalGraph.nodeLevel(node);
            } else {
                int bestForce = Integer.MAX_VALUE;
                int bestPosition = -1;
                int force = 0;
                for (int i = 1; i < forces.length; i++) {
                    if (forces[i] == 0) {
                        for (int j = 0; j < forces.length; j++) {
                            if (i != j) {
                                force += forces[j] - Math.abs(i - j);
                            }
                        }
                        if (force < bestForce) {
                            bestForce = force;
                            bestPosition = i;
                        }
                    }
                }
                goalGraph.moveNode(node, bestPosition);
                forces[bestPosition] = goalGraph.nodeLevel(node);
            }
            nodeEdges = goalGraph.nodeEdges(node);
            int crossNumA;
            int crossNumB;
            for (Integer edge : nodeEdges) {
                if (edges[edge][2] == -1) {
                    goalGraph.directionEdge(edge, direction);
                    crossNumA = goalGraph.crossingNumber();
                    goalGraph.invertEdge(edge);
                    crossNumB = goalGraph.crossingNumber();
                    if (crossNumB >= crossNumA) {
                        goalGraph.invertEdge(edge);
                    }
                }
            }
            direction = 1 - direction;
        }
        int crossNumA;
        int crossNumB;
        int leftPointer = 0;
        int rightPointer = cantNodes - 1;
        boolean pointer = true;
        int[] nodes = goalGraph.getNodes();
        for (int i = 0; i < nodes.length; i++) {
            if (pointer) {
                nodeEdges = goalGraph.nodeEdges(nodes[leftPointer]);
                leftPointer++;
                pointer = false;
            } else {
                nodeEdges = goalGraph.nodeEdges(nodes[rightPointer]);
                rightPointer--;
                pointer = true;
            }
            for (Integer edge : nodeEdges) {
                crossNumA = goalGraph.crossingNumber();
                goalGraph.invertEdge(edge);
                crossNumB = goalGraph.crossingNumber();
                if (crossNumB >= crossNumA) {
                    goalGraph.invertEdge(edge);
                }
            }
        }
        //ArcGraph optimizedGraph = completeGraphOptimization(goalGraph);
        return goalGraph;
    }

    public static ArcDiagram geneticOptimization(ArcDiagram graph) {
        //grafo resultado
        ArcDiagram goalGraph = graph;
        //da un tope de grafos de 100 para lanzar el algoritmo genético
        //debido al excesivo tiempo que tardará
        if (graph.getNodes().length <= 100) {
            //evalua el tiempo del algoritmo
            double startTime = System.currentTimeMillis();
            ArcDiagram newGoalGraph;
            //verifica que se encontró un punto fijo
            boolean fixPoint = false;
            int i = 0;
            //hara un máximo de 100 ciclos o se detendrá al encontrar un punto fijo
            do {
                //llama a un ciclo de generaciones
                newGoalGraph = geneticCycle(goalGraph);
                //verifica punto fijo
                if (goalGraph.crossingNumber() == newGoalGraph.crossingNumber()) {
                    fixPoint = true;
                }
                goalGraph = newGoalGraph;
                i++;
            } while (!fixPoint && i < 100);
            double endTime = (System.currentTimeMillis() - startTime) / 1000;
            timeGenetic = endTime;
            cycles = i;
        }
        return goalGraph;
    }

    private static ArcDiagram geneticCycle(ArcDiagram graph) {
        int generation = 0;
        boolean goal = false;
        ArcDiagram goalGraph = graph;
        LinkedList<ArcDiagram> population;
        Random random = new Random();
        int i;
        //crossing number para comparar y reducir población
        int originalCrossingNumber = graph.crossingNumber();
        //genera la población inicial con el grafo original
        population = generatePopulation(graph);
        //hará 1000 generaciones o se detendrá al encontrar un grafo sin cruces
        do {
            i = 0;
            ArcDiagram individual;
            //muta cada individuo y corta si encuentra uno sin cruces
            while (i < population.size() && !goal) {
                individual = population.get(i);
                //muta sus nodos con probabilidad 30%
                if (random.nextDouble() < 0.3) {
                    swapNodeMutation(individual);
                }
                //muta sus arcos con probabilidad 10%
                if (random.nextDouble() < 0.1) {
                    invertEdgeMutation(individual);
                }
                //System.out.println("    Individual CN: " + individual.crossingNumber());
                //verifica si es un grafo sin cruces
                if (individual.crossingNumber() == 0) {
                    goal = true;
                    goalGraph = individual.clone();
                    //System.out.println("    Goal: " + individual.toString());
                } else {
                    //verifica que sea mejor que el original o sino lo elimina de la población
                    if (originalCrossingNumber < individual.crossingNumber()) {
                        population.remove(individual);
                    }
                }
                i++;
            }
            //en caso de no encontrar un grafo sin cruces obtiene el mejor de la población
            if (!goal && population.size() != 0) {
                //los ordena por su fitness (evaluados por crossing number)
                Collections.sort(population, new Comparator<ArcDiagram>() {
                    @Override
                    public int compare(ArcDiagram o1, ArcDiagram o2) {
                        int result = 0;
                        if (o1.crossingNumber() < o2.crossingNumber()) {
                            result = -1;
                        } else if (o1.crossingNumber() > o2.crossingNumber()) {
                            result = 1;
                        }
                        return result;
                    }
                });

                //verifica que el grafo obtenido sea mejor que el obtenido en generaciones anteriores
                if (goalGraph.crossingNumber() > population.getFirst().crossingNumber()) {
                    goalGraph = population.getFirst().clone();
                }
            }
            generation++;
        } while (generation < 1000 && !goal);
        //System.out.println("    Population size: " + population.size());
        //System.out.println("    Best: " + goalGraph.toString());
        //System.out.println("    Generations: " + generation);
        return goalGraph;
    }

    private static LinkedList<ArcDiagram> generatePopulation(ArcDiagram graph) {
        LinkedList<ArcDiagram> population = new LinkedList<>();
        int numNodes = graph.getNodes().length;
        int numEdges = graph.getEdges().length;
        //para cada nodo realiza un intercambio con los restantes siguientes
        //y genera un nuevo individuo con tal cambio
        for (int i = 0; i < numNodes; i++) {
            for (int j = i + 1; j < numNodes; j++) {
                ArcDiagram neighbor = graph.clone();
                neighbor.swapNodes(i, j);
                population.add(neighbor);
            }
        }
        //para cada arco realiza su inversion y genera un nuevo individuo con el cambio
        for (int i = 0; i < numEdges; i++) {
            ArcDiagram neighbor = graph.clone();
            neighbor.invertEdge(i);
            population.add(neighbor);
        }
        return population;
    }

    private static void swapNodeMutation(ArcDiagram graph) {
        Random random = new Random();
        //obtiene un nodo aleatorio
        int nodeA = random.nextInt(graph.getNodes().length);
        int nodeB;
        //obtiene otro nodo aleatorio distinto del primero
        do {
            nodeB = random.nextInt(graph.getNodes().length);
        } while (nodeA == nodeB);
        //realiza el intercambio
        graph.swapNodes(nodeB, nodeB);
    }

    private static void invertEdgeMutation(ArcDiagram graph) {
        Random random = new Random();
        //obtiene un arco aleatorio
        int edge = random.nextInt(graph.getEdges().length);
        //realiza la inversión
        graph.invertEdge(edge);
    }

    public static ArcDiagram generateCompleteGraph(int numNodes) {
        Random random = new Random();
        int[] nodes = new int[numNodes];
        int cantEdges = 0;
        for (int i = 0; i < numNodes; i++) {
            nodes[i] = i;
            cantEdges += i;
        }
        int[][] edges = new int[cantEdges][3];
        int edgeIndex = 0;
        for (int i = 0; i < numNodes; i++) {
            for (int j = i + 1; j < numNodes; j++) {
                edges[edgeIndex][0] = i;
                edges[edgeIndex][1] = j;
                edges[edgeIndex][2] = random.nextInt(2);
                edgeIndex++;
            }
        }
        ArcDiagram completeGraph = new ArcDiagram(nodes, nodes, edges);
        return completeGraph;
    }

    public static ArcDiagram generateRandomGraph(int numNodes) {
        Random random = new Random();
        int[] nodes = new int[numNodes];
        int cantEdges[] = new int[numNodes];
        int totalCantEdges = 0;
        for (int i = 0; i < numNodes; i++) {
            nodes[i] = i;
            if (i < numNodes - 1) {
                int maxEdges = numNodes - i - 1;
                if (maxEdges > 2) {
                    maxEdges = 2;
                }
                cantEdges[i] = random.nextInt(maxEdges) + 1;
            }
            totalCantEdges += cantEdges[i];
        }
        int[][] edges = new int[totalCantEdges][3];
        int edgeIndex = 0;
        for (int i = 0; i < numNodes; i++) {
            LinkedList<Integer> connectedNodes = new LinkedList();
            for (int j = 0; j < cantEdges[i]; j++) {
                edges[edgeIndex][0] = i;
                int randomNode;
                do {
                    randomNode = random.nextInt(numNodes);
                } while (randomNode <= i || connectedNodes.contains(randomNode));
                connectedNodes.add(randomNode);
                edges[edgeIndex][1] = randomNode;
                edges[edgeIndex][2] = random.nextInt(2);
                edgeIndex++;
            }
        }
        ArcDiagram randomGraph = new ArcDiagram(nodes, nodes, edges);
        return randomGraph;
    }

    public static ArcDiagram importGraph(File file) {
        ArcDiagram graph = null;
        SAXBuilder builder = new SAXBuilder();
        Document document;
        try {
            document = (Document) builder.build(file);
            Element root = document.getRootElement();
            Element nodesList = root.getChild("nodes");
            List nodesElements = nodesList.getChildren("node");
            int cantNodes = nodesElements.size();
            int[] nodes = new int[cantNodes];
            int[] order = new int[cantNodes];
            //leer nodos
            for (int i = 0; i < cantNodes; i++) {
                Element node = (Element) nodesElements.get(i);
                nodes[i] = Integer.parseInt(node.getText());
                order[nodes[i]] = Integer.parseInt(node.getAttributeValue("order"));
            }
            Element edgesList = root.getChild("edges");
            List edgesElements = edgesList.getChildren("edge");
            int cantEdges = edgesElements.size();
            int[][] edges = new int[cantEdges][3];
            //leer arcos
            for (int i = 0; i < cantEdges; i++) {
                Element edge = (Element) edgesElements.get(i);
                List edgeNodes = edge.getChildren("node");
                Element nodeA = (Element) edgeNodes.get(0);
                Element nodeB = (Element) edgeNodes.get(1);
                edges[i][0] = Integer.parseInt(nodeA.getText());
                edges[i][1] = Integer.parseInt(nodeB.getText());
                edges[i][2] = Integer.parseInt(edge.getAttributeValue("direction"));
            }
            graph = new ArcDiagram(nodes, order, edges);
        } catch (JDOMException | IOException ex) {
            System.err.println("Error de lectura de archivo XML.");
        }

        return graph;
    }

    public static Document exportArcGraph(ArcDiagram graph) {
        Element diagram = new Element("diagram");
        Document document = new Document(diagram);
        int[] nodes = graph.getNodes();
        int[][] edges = graph.getEdges();
        int[] order = graph.getOrder();
        Element nodesList = new Element("nodes");
        diagram.addContent(nodesList);
        //escribir nodos
        for (int i = 0; i < nodes.length; i++) {
            Element node = new Element("node").setText(nodes[i] + "").setAttribute("order", order[nodes[i]] + "");
            nodesList.addContent(node);
        }
        Element edgesList = new Element("edges");
        diagram.addContent(edgesList);
        //escribir arcos
        for (int i = 0; i < edges.length; i++) {
            Element edge = new Element("edge").setAttribute("direction", edges[i][2] + "");
            edge.addContent(new Element("node").setText(edges[i][0] + ""));
            edge.addContent(new Element("node").setText(edges[i][1] + ""));
            edgesList.addContent(edge);
        }
        return document;
    }

    public static int completeGraphCrossingNumber(int numNodes) {
        return (int) ((1.0 / 4.0) * Math.floor(numNodes / 2) * Math.floor((numNodes - 1) / 2) * Math.floor((numNodes - 2) / 2) * Math.floor((numNodes - 3) / 2));
    }

    public static void saveResult(int numNodes, int numEdges,
            int CNOri,
            int CNOriLin, double timeOriLin,
            int CNOriGen, double timeOriGen, int cyclesOriGen,
            int CNOriFor, double timeOriFor, double timeA2GOriFor,
            int CNOriLinGen, double timeOriLinGen, int cyclesOriLinGen,
            int CNOriLinFor, double timeOriLinFor, double timeA2GOriLinFor,
            int CNOriLinGenFor, double timeOriLinGenFor, double timeA2GOriLinGenFor,
            int CNOriGenFor, double timeOriGenFor, double timeA2GOriGenFor) {
        new File("test").mkdirs();
        String savePath = "test/results.xml";
        File register = new File(savePath);
        SAXBuilder builder = new SAXBuilder();
        Document document;
        try {
            Element graphResults;
            if (register.exists()) {
                document = (Document) builder.build(register);
                Element root = document.getRootElement();
                graphResults = root.getChild("n" + numNodes + "");
                if (graphResults == null) {
                    graphResults = new Element("n" + numNodes + "");
                    root.addContent(graphResults);
                }
            } else {
                Element root = new Element("results");
                document = new Document(root);
                graphResults = new Element("n" + numNodes + "");
                root.addContent(graphResults);
            }
            //List results = graphResults.getChildren("result");
            DecimalFormat df = new DecimalFormat("#.###");
            df.setRoundingMode(RoundingMode.CEILING);
            df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));

            Element result = new Element("result");
            result.setAttribute("numEdges", numEdges + "");
            result.setAttribute("CNOri", CNOri + "");
            result.setAttribute("CNOriLin", CNOriLin + "");
            result.setAttribute("CNOriGen", CNOriGen + "");
            result.setAttribute("CNOriFor", CNOriFor + "");
            result.setAttribute("CNOriLinGen", CNOriLinGen + "");
            result.setAttribute("CNOriLinFor", CNOriLinFor + "");
            result.setAttribute("CNOriLinGenFor", CNOriLinGenFor + "");
            result.setAttribute("CNOriGenFor", CNOriGenFor + "");
            result.setAttribute("timeOriLin", df.format(timeOriLin));
            result.setAttribute("timeOriGen", df.format(timeOriGen));
            result.setAttribute("timeOriFor", df.format(timeOriFor));
            result.setAttribute("timeOriLinGen", df.format(timeOriLinGen));
            result.setAttribute("timeOriLinFor", df.format(timeOriLinFor));
            result.setAttribute("timeOriLinGenFor", df.format(timeOriLinGenFor));
            result.setAttribute("timeOriGenFor", df.format(timeOriGenFor));
            result.setAttribute("timeA2GOriFor", df.format(timeA2GOriFor));
            result.setAttribute("timeA2GOriLinFor", df.format(timeA2GOriLinFor));
            result.setAttribute("timeA2GOriLinGenFor", df.format(timeA2GOriLinGenFor));
            result.setAttribute("timeA2GOriGenFor", df.format(timeA2GOriGenFor));
            result.setAttribute("cyclesOriGen", cyclesOriGen + "");
            result.setAttribute("cyclesOriLinGen", cyclesOriLinGen + "");

            graphResults.addContent(result);

            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(document, new FileWriter(savePath));
        } catch (JDOMException | IOException ex) {
            System.err.println("Error de lectura de archivo XML.");
        }
    }

    public static void savePromedies() {
        new File("test").mkdirs();
        String savePathPromedies = "test/results-promedies.xml";
        File registerPromedies = new File(savePathPromedies);
        SAXBuilder builder = new SAXBuilder();
        Document documentPromedies;
        try {
            if (registerPromedies.exists()) {
                documentPromedies = (Document) builder.build(registerPromedies);
            } else {
                Element root = new Element("results-promedies");
                documentPromedies = new Document(root);
            }
            Element rootPromedies = documentPromedies.getRootElement();

            try {
                String savePathResults = "test/results.xml";
                File registerResults = new File(savePathResults);
                Document documentResults;
                if (registerResults.exists()) {
                    documentResults = (Document) builder.build(registerResults);
                    Element rootResults = documentResults.getRootElement();

                    Element graphPromedies;
                    for (Element graphResults : rootResults.getChildren()) {
                        Element graphPromedie = new Element("result-promedie");
                        int sumNumEdges = 0;

                        int sumCNOri = 0;
                        int sumCNOriLin = 0;
                        int sumCNOriGen = 0;
                        int sumCNOriFor = 0;
                        int sumCNOriLinGen = 0;
                        int sumCNOriLinFor = 0;
                        int sumCNOriLinGenFor = 0;
                        int sumCNOriGenFor = 0;

                        double sumTimeOriLin = 0;
                        double sumTimeOriGen = 0;
                        double sumTimeOriFor = 0;
                        double sumTimeOriLinGen = 0;
                        double sumTimeOriLinFor = 0;
                        double sumTimeOriLinGenFor = 0;
                        double sumTimeOriGenFor = 0;

                        int sumCyclesOriGen = 0;
                        int sumCyclesOriLinGen = 0;

                        double sumTimeA2GOriFor = 0;
                        double sumTimeA2GOriLinFor = 0;
                        double sumTimeA2GOriLinGenFor = 0;
                        double sumTimeA2GOriGenFor = 0;

                        for (Element graphResult : graphResults.getChildren()) {
                            sumNumEdges += graphResult.getAttribute("numEdges").getIntValue();

                            sumCNOri += graphResult.getAttribute("CNOri").getIntValue();
                            sumCNOriLin += graphResult.getAttribute("CNOriLin").getIntValue();
                            sumCNOriGen += graphResult.getAttribute("CNOriGen").getIntValue();
                            sumCNOriFor += graphResult.getAttribute("CNOriFor").getIntValue();
                            sumCNOriLinGen += graphResult.getAttribute("CNOriLinGen").getIntValue();
                            sumCNOriLinFor += graphResult.getAttribute("CNOriLinFor").getIntValue();
                            sumCNOriLinGenFor += graphResult.getAttribute("CNOriLinGenFor").getIntValue();
                            sumCNOriGenFor += graphResult.getAttribute("CNOriGenFor").getIntValue();

                            sumTimeOriLin += graphResult.getAttribute("timeOriLin").getDoubleValue();
                            sumTimeOriGen += graphResult.getAttribute("timeOriGen").getDoubleValue();
                            sumTimeOriFor += graphResult.getAttribute("timeOriFor").getDoubleValue();
                            sumTimeOriLinGen += graphResult.getAttribute("timeOriLinGen").getDoubleValue();
                            sumTimeOriLinFor += graphResult.getAttribute("timeOriLinFor").getDoubleValue();
                            sumTimeOriLinGenFor += graphResult.getAttribute("timeOriLinGenFor").getDoubleValue();
                            sumTimeOriGenFor += graphResult.getAttribute("timeOriGenFor").getDoubleValue();

                            sumCyclesOriGen += graphResult.getAttribute("cyclesOriGen").getIntValue();
                            sumCyclesOriLinGen += graphResult.getAttribute("cyclesOriLinGen").getIntValue();

                            sumTimeA2GOriFor += graphResult.getAttribute("timeA2GOriFor").getDoubleValue();
                            sumTimeA2GOriLinFor += graphResult.getAttribute("timeA2GOriLinFor").getDoubleValue();
                            sumTimeA2GOriLinGenFor += graphResult.getAttribute("timeA2GOriLinGenFor").getDoubleValue();
                            sumTimeA2GOriGenFor += graphResult.getAttribute("timeA2GOriGenFor").getDoubleValue();
                        }
                        DecimalFormat df = new DecimalFormat("#.###");
                        df.setRoundingMode(RoundingMode.CEILING);
                        df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));

                        int numResults = graphResults.getChildren().size();
                        graphPromedie.setAttribute("numEdges", sumNumEdges / numResults + "");
                        graphPromedie.setAttribute("CNOri", sumCNOri / numResults + "");
                        graphPromedie.setAttribute("CNOriLin", sumCNOriLin / numResults + "");
                        graphPromedie.setAttribute("CNOriGen", sumCNOriGen / numResults + "");
                        graphPromedie.setAttribute("CNOriFor", sumCNOriFor / numResults + "");
                        graphPromedie.setAttribute("CNOriLinGen", sumCNOriLinGen / numResults + "");
                        graphPromedie.setAttribute("CNOriLinFor", sumCNOriLinFor / numResults + "");
                        graphPromedie.setAttribute("CNOriLinGenFor", sumCNOriLinGenFor / numResults + "");
                        graphPromedie.setAttribute("CNOriGenFor", sumCNOriGenFor / numResults + "");
                        graphPromedie.setAttribute("timeOriLin", df.format(sumTimeOriLin / numResults) + "");
                        graphPromedie.setAttribute("timeOriGen", df.format(sumTimeOriGen / numResults) + "");
                        graphPromedie.setAttribute("timeOriFor", df.format(sumTimeOriFor / numResults) + "");
                        graphPromedie.setAttribute("timeOriLinGen", df.format(sumTimeOriLinGen / numResults) + "");
                        graphPromedie.setAttribute("timeOriLinFor", df.format(sumTimeOriLinFor / numResults) + "");
                        graphPromedie.setAttribute("timeOriLinGenFor", df.format(sumTimeOriLinGenFor / numResults) + "");
                        graphPromedie.setAttribute("timeOriGenFor", df.format(sumTimeOriGenFor / numResults) + "");
                        graphPromedie.setAttribute("timeA2GOriFor", df.format(sumTimeA2GOriFor / numResults) + "");
                        graphPromedie.setAttribute("timeA2GOriLinFor", df.format(sumTimeA2GOriLinFor / numResults) + "");
                        graphPromedie.setAttribute("timeA2GOriLinGenFor", df.format(sumTimeA2GOriLinGenFor / numResults) + "");
                        graphPromedie.setAttribute("timeA2GOriGenFor", df.format(sumTimeA2GOriGenFor / numResults) + "");
                        graphPromedie.setAttribute("cyclesOriGen", sumCyclesOriGen / numResults + "");
                        graphPromedie.setAttribute("cyclesOriLinGen", sumCyclesOriLinGen / numResults + "");

                        if (rootPromedies.getChild(graphResults.getName()) == null) {
                            graphPromedies = new Element(graphResults.getName());
                            rootPromedies.addContent(graphPromedies);
                        } else {
                            graphPromedies = rootPromedies.getChild(graphResults.getName());
                        }

                        if (graphPromedies.getChildren().size() > 0) {
                            graphPromedies.removeChild(graphPromedies.getChildren().get(0).getName());
                        }
                        graphPromedies.addContent(graphPromedie);
                    }

                    XMLOutputter xmlOutput = new XMLOutputter();
                    xmlOutput.setFormat(Format.getPrettyFormat());
                    xmlOutput.output(documentPromedies, new FileWriter(savePathPromedies));
                }
            } catch (JDOMException | IOException ex) {
                System.err.println("Error de lectura/generacion de archivo XML de resultados.\n(" + ex.getMessage() + ")");
            }
        } catch (JDOMException | IOException ex) {
            System.err.println("Error de lectura/generacion de archivo XML de promedios.\n(" + ex.getMessage() + ")");
        }
    }

    public static double consultPTime(int numNodes) {
        String savePath = "test/results.xml";
        File register = new File(savePath);
        SAXBuilder builder = new SAXBuilder();
        Document document;
        double sumTimes = 0;
        int numTests = 0;
        double promTimes = -1.0;
        try {
            Element graphResults;
            if (register.exists()) {
                document = (Document) builder.build(register);
                Element root = document.getRootElement();
                graphResults = root.getChild("n" + numNodes + "");
                if (graphResults != null) {
                    List<Element> results = graphResults.getChildren("result");
                    for (Element result : results) {
                        sumTimes += Double.parseDouble(result.getAttributeValue("time"));
                        numTests++;
                    }
                    promTimes = sumTimes / numTests;
                }
            }
        } catch (JDOMException | IOException ex) {
            System.err.println("Error de lectura de archivo XML.");
        }
        return promTimes;
    }

    public static int consultPCrycle(int numNodes) {
        String savePath = "test/results.xml";
        File register = new File(savePath);
        SAXBuilder builder = new SAXBuilder();
        Document document;
        int sumCycles = 0;
        int numTests = 0;
        int promCycles = -1;
        try {
            Element graphResults;
            if (register.exists()) {
                document = (Document) builder.build(register);
                Element root = document.getRootElement();
                graphResults = root.getChild("n" + numNodes + "");
                if (graphResults != null) {
                    List<Element> results = graphResults.getChildren("result");
                    for (Element result : results) {
                        sumCycles += Integer.parseInt(result.getAttributeValue("cycles"));
                        numTests++;
                    }
                    promCycles = sumCycles / numTests;
                }
            }
        } catch (JDOMException | IOException ex) {
            System.err.println("Error de lectura de archivo XML.");
        }
        return promCycles;
    }

    public static void calculateResults(int numNodes, int numTests) {
        //calcular los crossing number, tiempos y ciclos de todos los posibles 
        //pasos desde el grafo original, el grafo optimizado lineal, 
        //el grafo optimizado genetico y grafo optimizado por fuerzas
        ArcDiagram graphOri;
        ArcDiagram graphOriLin;
        ArcDiagram graphOriGen;
        Graph<Vertex, Edge> graphOriFor;
        ArcDiagram graphOriLinGen;
        Graph<Vertex, Edge> graphOriLinFor;
        Graph<Vertex, Edge> graphOriLinGenFor;
        Graph<Vertex, Edge> graphOriGenFor;

        int numEdges = 0;

        int CNOri = 0;
        int CNOriLin = 0;
        int CNOriGen = 0;
        int CNOriFor = 0;
        int CNOriLinGen = 0;
        int CNOriLinFor = 0;
        int CNOriLinGenFor = 0;
        int CNOriGenFor = 0;

        double timeOriLin = 0;
        double timeOriGen = 0;
        double timeOriFor = 0;
        double timeOriLinGen = 0;
        double timeOriLinFor = 0;
        double timeOriLinGenFor = 0;
        double timeOriGenFor = 0;

        int cyclesOriGen = 0;
        int cyclesOriLinGen = 0;

        double timeA2GOriFor = 0;
        double timeA2GOriLinFor = 0;
        double timeA2GOriLinGenFor = 0;
        double timeA2GOriGenFor = 0;

        System.out.println("Calculando " + numTests + " grafos de " + numNodes + " nodos...");
        System.out.print("[");
        for (int i = 0; i < numTests; i++) {
            try {
                graphOri = generateRandomGraph(numNodes);
                CNOri = graphOri.crossingNumber();

                numEdges = graphOri.getEdges().length;

                graphOriLin = linearOptimization(graphOri);
                CNOriLin = graphOriLin.crossingNumber();
                timeOriLin = timeLineal;

                graphOriGen = geneticOptimization(graphOri);
                CNOriGen = graphOriGen.crossingNumber();
                timeOriGen = timeGenetic;
                cyclesOriGen = cycles;

                graphOriFor = arcDiagramToGenericGraph(graphOri, new HashMap<>(), 100);
                simulateForceDirected(graphOriFor);
                CNOriFor = crossingNumberGeneric(graphOriFor);
                timeA2GOriFor = timeArcDiagram2Generic;
                timeOriFor = timeForceDirected + timeA2GOriFor;

                graphOriLinGen = geneticOptimization(graphOriLin);
                CNOriLinGen = graphOriLinGen.crossingNumber();
                timeOriLinGen = timeGenetic + timeOriLin;
                cyclesOriLinGen = cycles;

                graphOriLinFor = arcDiagramToGenericGraph(graphOriLin, new HashMap<>(), 100);
                simulateForceDirected(graphOriLinFor);
                CNOriLinFor = crossingNumberGeneric(graphOriLinFor);
                timeA2GOriLinFor = timeArcDiagram2Generic;
                timeOriLinFor = timeForceDirected + timeA2GOriLinFor + timeOriLin;

                graphOriLinGenFor = arcDiagramToGenericGraph(graphOriLinGen, new HashMap<>(), 100);
                simulateForceDirected(graphOriLinGenFor);
                CNOriLinGenFor = crossingNumberGeneric(graphOriLinGenFor);
                timeA2GOriLinGenFor = timeArcDiagram2Generic;
                timeOriLinGenFor = timeForceDirected + timeA2GOriLinGenFor + timeOriLinGen;

                graphOriGenFor = arcDiagramToGenericGraph(graphOriGen, new HashMap<>(), 100);
                simulateForceDirected(graphOriGenFor);
                CNOriGenFor = crossingNumberGeneric(graphOriGenFor);
                timeA2GOriGenFor = timeArcDiagram2Generic;
                timeOriGenFor = timeForceDirected + timeA2GOriGenFor + timeOriGen;
                
                saveResult(numNodes, numEdges,
                        CNOri,
                        CNOriLin, timeOriLin,
                        CNOriGen, timeOriGen, cyclesOriGen,
                        CNOriFor, timeOriFor, timeA2GOriFor,
                        CNOriLinGen, timeOriLinGen, cyclesOriLinGen,
                        CNOriLinFor, timeOriLinFor, timeA2GOriLinFor,
                        CNOriLinGenFor, timeOriLinGenFor, timeA2GOriLinGenFor,
                        CNOriGenFor, timeOriGenFor, timeA2GOriGenFor
                );

                if (i % (numTests / 10) == 0) {
                    System.out.print("*");
                }
            } catch (Exception ex) {
                System.err.println("No se han podido computar los layouts.\n(" + ex.getMessage() + ")");
                ex.printStackTrace();
            }
        }
        System.out.println("]");

        savePromedies();
    }

    public static int crossingNumberGeneric(Graph<Vertex, Edge> graph) {
        Edge[] edges = (Edge[]) graph.edgeSet().toArray(new Edge[graph.edgeSet().size()]);
        int crossingNumber = 0;
        for (int i = 0; i < edges.length; i++) {
            for (int j = i; j < edges.length; j++) {
                if (!edges[i].getV().equals(edges[j].getV()) && !edges[i].getV().equals(edges[j].getU())
                        && !edges[i].getU().equals(edges[j].getU()) && !edges[i].getU().equals(edges[j].getV())) {
                    Line2D lineA = new Line2D.Double(edges[i].getV().getPos().x, edges[i].getV().getPos().y,
                            edges[i].getU().getPos().x, edges[i].getU().getPos().y);
                    Line2D lineB = new Line2D.Double(edges[j].getV().getPos().x, edges[j].getV().getPos().y,
                            edges[j].getU().getPos().x, edges[j].getU().getPos().y);
                    crossingNumber = crossingNumber + (lineA.intersectsLine(lineB) ? 1 : 0);
                }
            }
        }
        return crossingNumber;
    }
}
