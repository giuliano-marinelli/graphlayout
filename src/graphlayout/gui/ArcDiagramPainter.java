package graphlayout.gui;

import fdp.graph.Edge;
import fdp.graph.Vertex;
import graphlayout.GraphLayout;
import static graphlayout.GraphLayout.simulateForceDirected;
import graphlayout.arcdiagram.ArcDiagram;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jgrapht.Graph;
import static graphlayout.GraphLayout.initSimulateConcentricDirected;
import java.awt.Font;

/**
 *
 * @author Giuliano Marinelli
 */
public class ArcDiagramPainter extends JFrame implements ActionListener {

    private ArcDiagram graph;
    private JPanel pnl = new JPanel();
    private JButton btnRandomGraph = new JButton("Aleatorio");
    private JButton btnInvertNodes = new JButton("Inter. Nodos");
    private JButton btnInvertCurve = new JButton("Inver. Arco");
    private JButton btnGeneticOpt = new JButton("Alg.Genetico");
    private JButton btnLinealOpt = new JButton("Alg.G.Comp");
    private JButton btnCompleteGraph = new JButton("Completo");
    private JButton btnExportGraph = new JButton("Exportar");
    private JButton btnImportGraph = new JButton("Importar");
    private JButton btnForcesOpt = new JButton("Alg.Fuerzas");
    private JButton btnShowGeneric = new JButton("Generico");
    private JTextField tfInvertOne = new JTextField("1");
    private JTextField tfInvertTwo = new JTextField("2");
    private JTextField tfAmountNodes = new JTextField("5");
    private JLabel lbCrossingNumber = new JLabel("CN: ");
    private JLabel lbCrossingNumberComplete = new JLabel("");
    private JLabel lbPopulation = new JLabel("");
    private JLabel lbEstimatedTime = new JLabel("");
    private JLabel lbRealTime = new JLabel("");
    private JLabel lbEstimatedCycles = new JLabel("");
    private JLabel lbRealCycles = new JLabel("");
    private JLabel lbAmountNodes = new JLabel("<html>Cant.<br>Nodos</html>");
    private JLabel lbInvertOne = new JLabel("Nodo A");
    private JLabel lbInvertTwo = new JLabel("Nodo B");
    private boolean safeResult = false;

    public ArcDiagramPainter(ArcDiagram graph) {
        this.graph = graph;
        setTitle("ArcGraph Painter");
        setSize(600, 700);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        btnRandomGraph.setBounds(15, 10, 85, 20);
        btnRandomGraph.addActionListener(this);
        btnInvertNodes.setBounds(115, 10, 85, 20);
        btnInvertNodes.addActionListener(this);
        btnInvertNodes.setFont(new Font("Dialog", Font.BOLD, 8));
        btnInvertCurve.setBounds(205, 10, 85, 20);
        btnInvertCurve.addActionListener(this);
        btnInvertCurve.setFont(new Font("Dialog", Font.BOLD, 9));
        btnGeneticOpt.setBounds(300, 10, 85, 20);
        btnGeneticOpt.addActionListener(this);
        btnGeneticOpt.setFont(new Font("Dialog", Font.BOLD, 8));
        btnLinealOpt.setBounds(300, 35, 85, 20);
        btnLinealOpt.addActionListener(this);
        btnLinealOpt.setFont(new Font("Dialog", Font.BOLD, 8));
        btnCompleteGraph.setBounds(15, 35, 85, 20);
        btnCompleteGraph.addActionListener(this);
        btnCompleteGraph.setFont(new Font("Dialog", Font.BOLD, 10));
        btnExportGraph.setBounds(400, 10, 85, 20);
        btnExportGraph.addActionListener(this);
        btnImportGraph.setBounds(400, 35, 85, 20);
        btnImportGraph.addActionListener(this);
        btnForcesOpt.setBounds(490, 35, 85, 20);
        btnForcesOpt.addActionListener(this);
        btnForcesOpt.setFont(new Font("Dialog", Font.BOLD, 8));
        btnShowGeneric.setBounds(490, 60, 85, 20);
        btnShowGeneric.addActionListener(this);
        //btnShowGeneric.setFont(new Font("Dialog", Font.BOLD, 8));
        tfInvertOne.setBounds(160, 35, 40, 20);
        tfInvertTwo.setBounds(250, 35, 40, 20);
        tfAmountNodes.setBounds(60, 60, 40, 20);
        tfAmountNodes.setText("" + graph.getNodes().length);
        lbCrossingNumber.setBounds(500, 10, 85, 20);
        lbCrossingNumberComplete.setBounds(500, 35, 85, 20);
        lbPopulation.setBounds(15, 600, 190, 20);
        lbEstimatedTime.setBounds(175, 600, 190, 20);
        lbRealTime.setBounds(175, 625, 190, 20);
        lbEstimatedCycles.setBounds(400, 600, 115, 20);
        lbRealCycles.setBounds(400, 625, 115, 20);
        lbAmountNodes.setBounds(15, 60, 40, 20);
        lbAmountNodes.setFont(new Font("Dialog", Font.PLAIN, 9));
        lbInvertOne.setBounds(115, 35, 40, 20);
        lbInvertOne.setFont(new Font("Dialog", Font.PLAIN, 9));
        lbInvertTwo.setBounds(205, 35, 40, 20);
        lbInvertTwo.setFont(new Font("Dialog", Font.PLAIN, 9));

        pnl.setBounds(0, 0, 600, 200);
        pnl.setBounds(0, 200, 600, 100);
        pnl.setLayout(null);
        pnl.add(btnRandomGraph);
        pnl.add(btnInvertNodes);
        pnl.add(btnInvertCurve);
        pnl.add(btnGeneticOpt);
        pnl.add(btnLinealOpt);
        pnl.add(btnCompleteGraph);
        pnl.add(btnExportGraph);
        pnl.add(btnImportGraph);
        pnl.add(btnForcesOpt);
        pnl.add(btnShowGeneric);
        pnl.add(tfInvertOne);
        pnl.add(tfInvertTwo);
        pnl.add(tfAmountNodes);
        pnl.add(lbCrossingNumber);
        pnl.add(lbCrossingNumberComplete);
        pnl.add(lbPopulation);
        pnl.add(lbEstimatedTime);
        pnl.add(lbRealTime);
        pnl.add(lbEstimatedCycles);
        pnl.add(lbRealCycles);
        pnl.add(lbAmountNodes);
        pnl.add(lbInvertOne);
        pnl.add(lbInvertTwo);
        add(pnl);
        
        this.repaint();
    }

    public ArcDiagramPainter(ArcDiagram graph, Component c) {
        this(graph);
        c.setLocation(c.getX() - 300, c.getY());
        setLocationRelativeTo(c);
        setLocation(getX() + 600, getY());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        lbCrossingNumberComplete.setText("");
        lbRealTime.setText("");
        lbRealCycles.setText("");
        if (e.getSource() == btnRandomGraph) {
            graph = GraphLayout.generateRandomGraph(Integer.parseInt(tfAmountNodes.getText()));
            //graph = GraphLayout.linearOptimization(graph);
            safeResult = true;
        } else if (e.getSource() == btnLinealOpt) {
            //graph = GraphLayout.generateRandomGraph(Integer.parseInt(tfAmountNodes.getText()));
            graph = GraphLayout.linearOptimization(graph);
            safeResult = true;
        } else if (e.getSource() == btnInvertNodes) {
            graph.swapNodes(Integer.parseInt(tfInvertOne.getText()), Integer.parseInt(tfInvertTwo.getText()));
        } else if (e.getSource() == btnInvertCurve) {
            graph.invertEdge(Integer.parseInt(tfInvertOne.getText()), Integer.parseInt(tfInvertTwo.getText()));
        } else if (e.getSource() == btnGeneticOpt) {
            int crossNumIni = graph.crossingNumber();
            graph = GraphLayout.geneticOptimization(graph);
            if (graph.getNodes().length > 100) {
                showMessageDialog(this, "El algoritmo genético tardará demasiado al superar los 100 nodos.");
            } else {
                lbRealTime.setText("R.Time: " + GraphLayout.getTime() + "seg");
                lbRealCycles.setText("R.Cycles: " + GraphLayout.getCycles());
                if (safeResult) {
                    //GraphLayout.saveResult(graph, crossNumIni);
                    safeResult = false;
                }
            }
        } else if (e.getSource() == btnCompleteGraph) {
            graph = GraphLayout.generateCompleteGraph(Integer.parseInt(tfAmountNodes.getText()));
            //graph = GraphLayout.linearOptimization(graph);
            lbCrossingNumberComplete.setText("REAL: " + GraphLayout.completeGraphCrossingNumber(graph.getNodes().length));
        } else if (e.getSource() == btnExportGraph) {
            Document xml = GraphLayout.exportArcGraph(graph);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml");
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(filter);
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
            int result = fileChooser.showSaveDialog(this);
            String savePath = "";
            if (result == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                savePath = file.getAbsolutePath();
                if (!savePath.endsWith(".xml")) {
                    savePath = savePath + ".xml";
                }
                try {
                    XMLOutputter xmlOutput = new XMLOutputter();
                    xmlOutput.setFormat(Format.getPrettyFormat());
                    xmlOutput.output(xml, new FileWriter(savePath));
                } catch (IOException ex) {
                    System.err.println("Error de escritura de archivo XML.");
                }
            }
        } else if (e.getSource() == btnImportGraph) {
            FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml");
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(filter);
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                graph = GraphLayout.importGraph(file);
            }
            //System.out.println("    " + graph.toString());
            //System.out.println("CROSSING NUMBER [IMPORTED GRAPH] = " + graph.crossingNumber());
        } else if (e.getSource() == btnForcesOpt) {
            try {
                HashMap<Vertex, String> vertex2Nodes = new HashMap<>();
                Graph<Vertex, Edge> genericGraph = GraphLayout.arcDiagramToGenericGraph(graph, vertex2Nodes, 100);
                simulateForceDirected(genericGraph);
                int gridScale = 40;
//                Graph<Vertex, Edge> genericGraph = GraphLayout.arcDiagramToOrthogonalGraph(graph, vertex2Nodes, gridScale);
//                HashMap<Point, Integer> grid = new HashMap<>();
//                Point gridCentroid = new Point();
//                initSimulateConcentricDirected(genericGraph, grid, gridCentroid, gridScale / 2);
//                new GenericGraphPainter(genericGraph, vertex2Nodes, grid, gridCentroid, gridScale / 2, this).setVisible(true);
                new GenericGraphPainter(genericGraph, vertex2Nodes, null, null, gridScale / 2, this).setVisible(true);
            } catch (Exception ex) {
                showMessageDialog(this, ex.getMessage().toString());
            }
        } else if (e.getSource() == btnShowGeneric) {
            try {
                HashMap<Vertex, String> vertex2Nodes = new HashMap<>();
                Graph<Vertex, Edge> genericGraph = GraphLayout.arcDiagramToGenericGraph(graph, vertex2Nodes, 100);
                int gridScale = 40;
                new GenericGraphPainter(genericGraph, vertex2Nodes, null, null, gridScale / 2, this).setVisible(true);
            } catch (Exception ex) {
                showMessageDialog(this, ex.getMessage().toString());
            }
        }
        lbCrossingNumber.setText("CN: " + graph.crossingNumber());
        int numNodes = graph.getNodes().length;
        int numEdges = graph.getEdges().length;
        int population = numEdges;
        for (int j = 1; j < numNodes; j++) {
            population += j;
        }
//        lbPopulation.setText("Population: " + population);
//        double estimatedTime = GraphLayout.consultPTime(numNodes);
//        if (estimatedTime != -1) {
//            lbEstimatedTime.setText("E.Time: " + new DecimalFormat("0.0000").format(estimatedTime));
//        } else {
//            lbEstimatedTime.setText("E.Time: -");
//        }
//        int estimatedCycles = GraphLayout.consultPCrycle(numNodes);
//        if (estimatedCycles != -1) {
//            lbEstimatedCycles.setText("E.Cycles: " + estimatedCycles);
//        } else {
//            lbEstimatedCycles.setText("E.Cycles: -");
//        }
        //showMessageDialog(null, resultMsg);
        //System.out.println();
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, (int) getWidth(), (int) getHeight());

        btnRandomGraph.repaint();
        btnInvertNodes.repaint();
        btnInvertCurve.repaint();
        btnGeneticOpt.repaint();
        btnLinealOpt.repaint();
        btnCompleteGraph.repaint();
        btnExportGraph.repaint();
        btnImportGraph.repaint();
        btnForcesOpt.repaint();
        btnShowGeneric.repaint();
        tfInvertOne.repaint();
        tfInvertTwo.repaint();
        tfAmountNodes.repaint();
        lbCrossingNumber.repaint();
        lbCrossingNumberComplete.repaint();
        lbPopulation.repaint();
        lbEstimatedTime.repaint();
        lbRealTime.repaint();
        lbEstimatedCycles.repaint();
        lbRealCycles.repaint();
        lbAmountNodes.repaint();
        lbInvertOne.repaint();
        lbInvertTwo.repaint();

        LinkedList<Color> degreeColors = new LinkedList<>();
        degreeColors.add(Color.GREEN);
        degreeColors.add(Color.YELLOW);
        degreeColors.add(Color.ORANGE);
        degreeColors.add(Color.RED);
        degreeColors.add(Color.BLUE);
        degreeColors.add(Color.decode("#9900FF"));

        int degree = 0;
        for (Color degreeColor : degreeColors) {
            g.setColor(degreeColor);
            g.drawLine(30, 120 + (degree * 20), 100, 120 + (degree * 20));
            g.drawString(degree + 1 + "", 15, 120 + (degree * 20));
            degree++;
        }

        int[] nodes = graph.getNodes();
        int[] order = graph.getOrder();
        int[][] edges = graph.getEdges();
        for (int i = 0; i < edges.length; i++) {
            int edgeDegree = graph.edgeDegree(i);
            switch (edgeDegree) {
                case 1:
                    g.setColor(degreeColors.get(0));
                    break;
                case 2:
                    g.setColor(degreeColors.get(1));
                    break;
                case 3:
                    g.setColor(degreeColors.get(2));
                    break;
                case 4:
                    g.setColor(degreeColors.get(3));
                    break;
                case 5:
                    g.setColor(degreeColors.get(4));
                    break;
                case 6:
                    g.setColor(degreeColors.get(5));
                    break;
                default:
                    g.setColor(Color.BLACK);
                    break;
            }

            boolean isTop = (edges[i][2] == 1);
            int ordEdgeNodeA = order[edges[i][0]];
            int ordEdgeNodeB = order[edges[i][1]];
            int distance = Math.abs(ordEdgeNodeA - ordEdgeNodeB) * getWidth() / (nodes.length + 1);
            int x = (Math.min(ordEdgeNodeA, ordEdgeNodeB) + 1) * getWidth() / (nodes.length + 1);
            int y = (getHeight() / 2) - (distance / 2);
            int startAng = 0;
            int topInvert = 1;
            if (!isTop) {
                startAng = 180;
                topInvert = -1;
            }

            if (Math.abs(ordEdgeNodeA - ordEdgeNodeB) < 2) {
                g.drawLine(x, (getHeight() / 2), x + getWidth() / (nodes.length + 1), (getHeight() / 2));
            } else {
                g.drawArc(x, y, distance, distance, startAng, 180);

                LinkedList<Integer> crossEdges = graph.crossingEdges(i);

                for (Integer crossEdge : crossEdges) {
                    //System.out.println("-----------");
                    boolean crossIsTop = (edges[crossEdge][2] == 1);
                    int crossOrdEdgeNodeA = order[edges[crossEdge][0]];
                    int crossOrdEdgeNodeB = order[edges[crossEdge][1]];
                    int crossDistance = Math.abs(crossOrdEdgeNodeA - crossOrdEdgeNodeB) * getWidth() / (nodes.length + 1);
                    int crossX = (Math.min(crossOrdEdgeNodeA, crossOrdEdgeNodeB) + 1) * getWidth() / (nodes.length + 1);
                    int crossY = (getHeight() / 2) - (crossDistance / 2);

                    double p0 = (x + distance / 2);
                    //System.out.println("P0=" + x + "+" + distance / 2 + "=" + p0);
                    double p1 = (crossX + crossDistance / 2);
                    //System.out.println("P1=" + crossX + "+" + crossDistance / 2 + "=" + p0);
                    double d = Math.abs(p0 - p1);
                    //System.out.println("d=" + d);
                    double a = (Math.pow(distance / 2, 2) - Math.pow(crossDistance / 2, 2) + Math.pow(d, 2)) / (2 * d);
                    //System.out.println("a=" + a);
                    double p2 = p0 + a * (p1 - p0) / d;
                    //System.out.println("P2=" + p2);
                    double h = Math.sqrt(Math.pow(distance / 2, 2) - Math.pow(a, 2));

                    g.setColor(Color.BLACK);
                    g.drawOval((int) p2 - 5, (int) ((getHeight() / 2) - h * topInvert - 5), 10, 10);
                }
            }
            for (int j = 0; j < nodes.length; j++) {
                g.setColor(Color.BLACK);
                g.drawOval((j + 1) * getWidth() / (nodes.length + 1) - 7, getHeight() / 2 - 7, 14, 14);
                g.drawString(nodes[j] + "", (j + 1) * getWidth() / (nodes.length + 1) - 3, getHeight() / 2 + 4);
            }
        }
    }

}
