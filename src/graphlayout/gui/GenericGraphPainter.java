package graphlayout.gui;

import fdp.graph.Edge;
import fdp.graph.Vertex;
import graphlayout.GraphLayout;
import static graphlayout.GraphLayout.completeSimulateConcentricDirected;
import static graphlayout.GraphLayout.iterateSimulateConcentricDirected;
import graphlayout.arcdiagram.ArcDiagram;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jgrapht.Graph;

/**
 *
 * @author Giuliano Marinelli
 */
public class GenericGraphPainter extends JFrame implements ActionListener {

    private Graph<Vertex, Edge> graph;
    private HashMap<Vertex, String> vertex2Nodes;
    private HashMap<Point, Integer> grid;
    private Point gridCentroid;
    private int gridScale;
    private JPanel pnl = new JPanel();
    private JLabel lbCrossingNumber = new JLabel("CN: ");
    private int crossingNumber = 0;
    private JButton btnIterateSimulation = new JButton("Iterate");
    private JButton btnCompleteSimulation = new JButton("Complete");

    public GenericGraphPainter(Graph<Vertex, Edge> graph, HashMap<Vertex, String> vertex2Nodes, HashMap<Point, Integer> grid, Point gridCentroid, int gridScale) {
        this.graph = graph;
        this.vertex2Nodes = vertex2Nodes;
        this.grid = grid;
        this.gridCentroid = gridCentroid;
        this.gridScale = gridScale;
        setTitle("Generic Graph Painter");
        setSize(600, 700);
        setLocationRelativeTo(null);
        setVisible(true);

        btnIterateSimulation.setBounds(15, 10, 85, 20);
        btnIterateSimulation.addActionListener(this);
        btnCompleteSimulation.setBounds(115, 10, 85, 20);
        btnCompleteSimulation.addActionListener(this);
        lbCrossingNumber.setBounds(500, 10, 85, 20);

        pnl.setBounds(0, 0, 600, 200);
        pnl.setBounds(0, 200, 600, 100);
        pnl.setLayout(null);
        pnl.add(btnIterateSimulation);
        pnl.add(btnCompleteSimulation);
        pnl.add(lbCrossingNumber);
        add(pnl);
    }

    public GenericGraphPainter(Graph<Vertex, Edge> graph, HashMap<Vertex, String> vertex2Nodes, HashMap<Point, Integer> grid, Point gridCentroid, int gridScale, Component c) {
        this(graph, vertex2Nodes, grid, gridCentroid, gridScale);
        //c.setLocation(c.getX() - 300, c.getY());
        setLocationRelativeTo(c);
        setLocation(getX() + 600, getY());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        lbCrossingNumber.setText("CN: " + crossingNumber);

        if (e.getSource() == btnIterateSimulation) {
            //iterateSimulateConcentricDirected(graph, grid, gridCentroid, gridScale);
        } else if (e.getSource() == btnCompleteSimulation) {
            //completeSimulateConcentricDirected(graph, grid, gridCentroid, gridScale);
        }
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, (int) getWidth(), (int) getHeight());

        btnIterateSimulation.repaint();
        btnCompleteSimulation.repaint();

        int xOffset = (int) getWidth() / 4;
        int yOffset = (int) getHeight() / 4;
        for (Vertex vertex : graph.vertexSet()) {
            if (!vertex2Nodes.get(vertex).contains("i")) {
                g.setColor(Color.BLUE);
                g.drawOval((int) vertex.getPos().x + xOffset, (int) vertex.getPos().y + yOffset, 14, 14);
                g.drawString(vertex2Nodes.get(vertex), (int) vertex.getPos().x + xOffset, (int) vertex.getPos().y + 4 + yOffset);
            } else {
                g.setColor(Color.RED);
                g.drawOval((int) vertex.getPos().x + xOffset + 3, (int) vertex.getPos().y + yOffset + 3, 7, 7);
            }
        }
        g.setColor(Color.RED);
        for (Edge edge : graph.edgeSet()) {
            //System.out.println(vertex2Nodes.get(edge.getV()) + " -> " + vertex2Nodes.get(edge.getU()));
            g.drawLine((int) edge.getV().getPos().x + 7 + xOffset, (int) edge.getV().getPos().y + 7 + yOffset,
                    (int) edge.getU().getPos().x + 7 + xOffset, (int) edge.getU().getPos().y + 7 + yOffset);
        }

        Edge[] edges = (Edge[]) graph.edgeSet().toArray(new Edge[graph.edgeSet().size()]);
        crossingNumber = 0;
        for (int i = 0; i < edges.length; i++) {
            for (int j = i; j < edges.length; j++) {
                if (!edges[i].getV().equals(edges[j].getV()) && !edges[i].getV().equals(edges[j].getU())
                        && !edges[i].getU().equals(edges[j].getU()) && !edges[i].getU().equals(edges[j].getV())) {
                    Line2D lineA = new Line2D.Double(edges[i].getV().getPos().x, edges[i].getV().getPos().y,
                            edges[i].getU().getPos().x, edges[i].getU().getPos().y);
                    Line2D lineB = new Line2D.Double(edges[j].getV().getPos().x, edges[j].getV().getPos().y,
                            edges[j].getU().getPos().x, edges[j].getU().getPos().y);
                    crossingNumber = crossingNumber + (lineA.intersectsLine(lineB) ? 1 : 0);
                }
            }
        }

//        g.setColor(Color.GREEN);
//        g.drawOval((int) gridCentroid.x + xOffset + 3, (int) gridCentroid.y + yOffset + 3, 7, 7);

//        for (Map.Entry<Point, Integer> gridEntry : this.grid.entrySet()) {
//            Point gridPoint = gridEntry.getKey();
//            g.setColor(Color.BLACK);
//            g.drawRect((int) gridPoint.x + xOffset, (int) gridPoint.y + yOffset, 14, 14);
//            g.setColor(Color.GRAY);
//            g.drawString(gridEntry.getValue().toString(), (int) gridPoint.x + xOffset, (int) gridPoint.y + yOffset + 7);
//        }

        g.setColor(Color.BLACK);
        //System.out.println("CN: " + crossingNumber);
        lbCrossingNumber.setText("CN: " + crossingNumber);
        lbCrossingNumber.repaint();
    }
}
